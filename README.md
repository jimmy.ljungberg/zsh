# Zsh

## Installation

```shell
sudo apt install zsh
```

## Konfiguration

### Byt standardshell till Zsh

```shell
chsh -s $(which zsh)
```

### Installera Oh My Zsh
```shell
sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"
```

Redigera *.zshrc* och välj sätt `ZSH_THEME="agnoster"`.
